# importamos la clase Flask de la libreria flask
# importamos render_template para poder renderizar html

from flask import Flask, render_template, request
import string
import random

app = Flask(__name__)

@app.route('/')
def form():
    return render_template('home.html')


@app.route('/', methods=['POST'])
def form_post():
    if request.method == 'POST':
        try:
            N = request.form['text']
            number = int(N)
            phrase = "Your password is:"
            test = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.ascii_lowercase + string.digits + string.punctuation) for _ in range(number))
            return render_template('home_gen.html', phrase = phrase, test = test)
        except:
            error_message = "Please enter length between 10-100"
            return render_template('home_gen.html', error_message = error_message)

@app.route('/about/')
def form_copy():
    return render_template('about.html')

if __name__ == "__main__":
    app.run(debug=True)
